<font color="#cf0505">*Change deck*</font>  : If "Yes", the card to which historical data is transferred is also put in the same deck as the card from which data have been taken.

<center><font color="#087f81">(Yes/No)</font></center>

<font color="#cf0505">*Delete old card*</font>  : If "Yes", the card from which properties and historical data have been taken is deleted after the transfer. Convenient if you use this add-on to merge two notes in one note with two cards, as I do.

<center><font color="#087f81">(Yes/No)</font></center>

<font color="#cf0505">*Shortcut : Copy*</font>  : the hotkey to be used once the first card (from which data have to be taken) is selected in the browser. I find "Ctrl+C" to be too simple for this deep operation (plus it is used by the *Copy Note* addon), "Ctrl+Shift+C" is already used for cloze deletion.

<center><font color="#087f81">("Ctrl+Alt+C", "Ctrl+C"...)</font></center>

<font color="#cf0505">*Shortcut : Paste*</font>  : the hotkey to be used once the second card (to which data have to be transfered) is selected in the browser. "Ctrl+V" and "Ctrl+Shift+V" are available.

<center><font color="#087f81">("Ctrl+Alt+V", "Ctrl+V"...)</font></center>

*For shortcuts, use any combination of Ctrl, Alt, Shift, keyboard characters.*
